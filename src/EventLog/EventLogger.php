<?php

declare(strict_types=1);

namespace Thrustbit\ModelEvent\EventLog;

use Thrustbit\ModelEvent\EventLog\Stream\Stream;

interface EventLogger
{
    public function create(Stream $stream): void;
}