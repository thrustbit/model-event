<?php

declare(strict_types=1);

namespace Thrustbit\ModelEvent\Model\Repository;

use Prooph\ServiceBus\EventBus;
use Thrustbit\ModelEvent\ModelRoot;

class ModelRepository implements Repository
{
    /**
     * @var EventBus
     */
    protected $eventBus;

    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function saveAggregateRoot(ModelRoot $root): void
    {
        $events = $root->popRecordedEvents();

        foreach ($events as $event) {
            $this->eventBus->dispatch($event);
        }
    }
}