<?php

declare(strict_types=1);

namespace Thrustbit\ModelEvent\Model\Repository;

use Thrustbit\ModelEvent\ModelRoot;

interface Repository
{
    public function saveAggregateRoot(ModelRoot $root): void;
}